const readLine = require('readline');

const interface = readLine.createInterface({
    input: process.stdin,
    output: process.stdout
});

function question(q) {
    return new Promise(resolve => {
        interface.question(q, (data) => {
            resolve(data);
        });
    });
}

async function penjumlahan() {
    let input1 = await question('Masukkan angka pertama : ');
    let input2 = await question('Masukkan angka kedua : ');


    let hasil = input1 + input2;
    console.log(hasil);
    interface.close();
}

async function main() {

    let mulai = await question(`Daftar menu kalkulator yang tersedia:
    1. Penambahan (+)
    2. Pengurangan (-)
    3. Perkalian (*)
    4. Pembagian (/)
    5. Akar Kuadrat (^)
    6. Luas Persegi
    7. Volume Kubus
    8. Volume Tabung
    Silahkan pilih nomor berapa : `
    );

    console.log('kamu pilih menu : ', mulai);
}

main();



// let input1, input2, hasil;

// function inputNumber(){
//     interface.question('Masukkan angka PERTAMA: ', function(input){
//         input1 = input;
//         interface.question('Masukkan angka KEDUA: ', function(input){
//             input2 = input;
//             interface.close();
//         });
//     });
// }

// function tambah(){
//     let input1, input2, hasil;

//     interface.question('Masukkan angka PERTAMA: ', function(input){
//         input1 = input;
//         interface.question('Masukkan angka KEDUA: ', function(input){
//             input2 = input;
//             interface.close();
//         });
//     });

//     hasil = input1 + input2;
//     console.log(`Hasil dari penjumlahan dua bilangan adalah: ${hasil}`);
// }

// function main(){
//     interface.question(
// `
// Halo pengguna!
// Selamat datang dalam program kalkulator v1!



// Masukkan menu yang akan dipilih (input berupa angka): `, function(input){
//             if(input == 1){
//                 //inputNumber();
//                 tambah();


//                 // const inputNumberPromise = new Promise((resolve) => {
//                 //     inputNumber();
//                 // });

//                 // inputNumberPromise.then(tambah());
//                 interface.close();
//             }
//         }
//     )
// }

// main();




// // interface.question('Masukkan nomor : ', function(dataDariUser){
// //     console.log(dataDariUser);
// //     interface.close();
// // });