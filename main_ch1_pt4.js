const { rejects } = require('assert');
const fs = require('fs');
const { resolve } = require('path');

// console.log('1');
// setTimeout(() => {console.log('2');}, 2000); 
// console.log('3');

// console.log('Buka Restoran');
// console.log('Mengambil Pesanan A');
// console.log('Mengambil Pesanan B');
// console.log('Mengambil Pesanan C');
// setTimeout(() => {console.log('Mengambil Pesanan A');}, 2000); 
// setTimeout(() => {console.log('Mengambil Pesanan B');}, 3000);
// setTimeout(() => {console.log('Mengambil Pesanan C');}, 4000);  
// console.log('Tutup Restoran');

// let nilai;

// setTimeout(() => {nilai = 9;}, 0);
// setTimeout(() => {console.log(nilai);}, 1000);

// console.log(nilai);

// let dataStudents = [];

// setTimeout(() => {dataStudents = ['student 1', 'student 2', 'student 3'];}, 0);

// console.log(dataStudents);

// const callback = (err, data) => {
//     //do process
//     if(!err){
//         console.log(data);
//     }
// };

// fs.readFile('text.txt', {encoding: 'utf-8'}, callback);

// console.log('Pertama');

// let students = [
//     {
//         'name':' jony'
//     },
//     {
//         'name':' rey'
//     }
// ]

// fs.writeFile('tulis.txt', JSON.stringify(students), {encoding: 'utf-8'}, () =>{
//     console.log('berhasil');
// });

// let data = fs.readFileSync('tulis.txt', {encoding: 'utf-8'});

// console.log('Pertama');
// console.log(data);
// console.log('Kedua');

// console.log('Pertama');
// setTimeout(() => {console.log('Kedua');}, 5000)
// console.log('Ketiga');

// let i = 0;

// setInterval(() => {
//     console.log('cetak ke -', i);
//     i++;
// }, 1000);

// let dataSiswa = ['sabrina', 'lutfi'];

// function cetakJumlahKata(arr, callback){
//     let newArr = [];

//     arr.forEach(el => {
//         newArr.push(callback(el));
//     });

//     return newArr;
// }

// let hasil = cetakJumlahKata(dataSiswa, (kata) => {
//     return kata.length;
// })

// console.log(hasil);

// const newPromise = new Promise(resolve => resolve('berhasil'));
// newPromise
//     .then(data => console.log(data)) //berhasil
//     .catch(err => console.log(err)); //error

// function isPasswordCorrect(password){
//     return new Promise((resolve, reject) => {
//         if(password === 'password123'){
//             resolve('Password cocok!');
//         } else {
//             reject('Password salah!');
//         }
//     })
// }

// isPasswordCorrect('password123')
//     .then(data => console.log(data))
//     .catch(err => console.log(err));

// function dapatDiBeli(uang){
//     return new Promise((resolve, reject) => {
//         if(uang > 5000 ){
//             resolve('Barang dapat dibeli!');
//         } else {
//             reject('Uang tidak mencukupi!');
//         }
//     })
// }

// async function main(){
//     try{
//         let hasil = await dapatDiBeli(6000);
//         console.log(hasil);
//     } catch(err) {
//         console.log(err);
//     }

// }
// main();

// function dapatDiBeli(uang){
//     return new Promise((resolve, reject) => {
//         if(uang > 5000 ){
//             resolve({
//                 err: '',
//                 data: 'Barang dapat dibeli!'
//             });
//         } else {
//             resolve({
//                 err: 'Uang tidak mencukupi!'
//             });
//         }
//     });
// }

// async function main(){
//     try{
//         let hasil = await dapatDiBeli(6000);
//         if(hasil.err){
//             console.log('errornya adalah', hasil.err);
//         } else{
//             console.log(hasil.data);
//         }
//     } catch(err) {
//         console.log(err);
//     }
// }
// main();

// class Human{
//     //static property
//     static isLivingOnEarth = true;

//     //static method
//     static walk(){
//         return 'sedang berjalan';
//     }

//     //instance property
//     constructor(name, address, password){
//         this.name = name;
//         this.address = address;
//         this.password = password;
//     }

//     //instance method
//     introduce(){
//         return 'Hello my name is ' + this.name;
//     }

//     checkPassword(password){
//         if(password == this.password){
//             return 'password benar';
//         } else{
//             return 'password salah';
//         }
//     }
// };

// Human.lunch = () => {
//     return 'having some lunch';
// };

// Human.prototype.greetings = (name) => {
//     return `Hi ${name}, my name is ${this.name}`;
// }

// console.log(Human.lunch())
// console.log(Human.isLivingOnEarth);
// console.log(Human.walk());
// console.log(Human.prototype.greetings('LOL'));

// let student = new Human('sabrina', 'jakarta', 'password123');
// console.log(student.name);
// console.log(student.introduce());
// console.log(student.checkPassword('password123'));

class Siswa{
    //static property
    static isActive = true;
    static isGraduate = false;

    //static method
    static study(){
        return 'sedang belajar';
    }
    static test(){
        return 'sedang menjalani tes'
    }

    //instance property
    constructor(nis, name, kelas){
        this.nis = nis;
        this.name = name;
        this.kelas = kelas;
    }

    //instance method
    introduce(){
        return 'Hello my name is ' + this.name;
    }

    onClass(kelas){
        kelas = this.kelas;
        return 'Aku dari kelas ' + kelas;
    }
};

let siswa = new Siswa('1234', 'Ivan', 'C');
console.log(Siswa.isActive);
console.log(Siswa.isGraduate);
console.log(siswa.introduce());
console.log(siswa.onClass('D'));
console.log(Siswa.study());
console.log(Siswa.test());

